## SpringBoot MybatisPlus无缝封装Redis BitMap

### 技术选型

- SpringBoot
- MybatisPlus
- Redis
- BitMap

### 方案优势

在MybatisPlus体系下，`透明化`解决缓存穿透问题，开发端`零感知`

### 知识储备

- SpringBoot简易入门
- MybatisPlus简易入门
- Redis BitMap简易入门
- Spring AOP简易入门

### 使用建议

- CURDer按照手册使用
- 面试提高篇：结合具体场景、分享技术理解，绝壁加分项，
- MybatisPlus重度使用者：源码分析 思考 why MybatisPlus can，why MyBatis can not.