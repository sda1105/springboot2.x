## 《SpringBoot  MybatisPlus实战合集》

### 前言

Spring、SpringBoot依然成为企业级开发事实上的标准，因此没有必要从原生Java开始撸代码，将主要经历放在SpringBoot体系上构建自己的知识与技术体系，无疑是较为明智的选择。

MybatisPlus作为国产优秀的DAO层访问框架，基本上能够覆盖企业级需求场景，遇到疑难杂症，也能退一步选择使用MyBatis来作为备选方案。因此如果要选择一门DAO层访问技术，那么首选MybatisPlus。
将MybatisPlus各种特性熟练掌握，能够极大增强市场竞争力。

尽管不少固执的程序员仍然坚守MyBatis，然而维护繁琐的XML文件，维护魔术词一样的SQL脚本无疑是在透支生命。可以遇见的是lambda版本的MybatisPlus市场份额将会持续增大，给作者点赞。
因此尽早将MybatisPlus融入自己的知识体系，尽早纳入自己的工具箱，用行动快人一步，不断积累与巩固持续性竞争力。

> 本合集立足SpringBoot讲解MybatisPlus实战

### 内容特色

##### 1、百分百立足实战场景

不同于隔靴搔痒、浅尝辄止的教程，本合集自始至终坚持`实战`特点，立足与`价值输出`，非常适合已经有工作经验的童鞋食用。

##### 2、全网唯一

本合集由`赛泰先生`原创性创作，全网仅此一份。不仅有配套视频讲解，抽丝剥茧，更有实战场景切入，学技术不再空洞。

本合集包含由`赛泰先生`原创性抽离、封装开源的第三方代码，通过抽离出工具类、工具方法，使用户在编写业务代码是能够真正感受一行代码解决实际问题的快感。

##### 3、创作优势

市面上的各类教程或多或少存在各类问题：

- 基础入门类视频时间长、吹水严重，讲得是皮毛，听着犯困

根本原因是创作者缺乏一线实战经验

- 有价值的实战内容藏得极其深，很难找到

有一线实战经验、并且技术能力过硬的开发者，常常不善表达，对已经入门，并且处于成长期的学习者非常不友好。

> 这是两种典型的矛盾场景：天天吹水、吹牛逼、段子满天飞的人不可能有技术深度；埋头干技术，不总结分享，不仰望天空，不与人沟通，也不知道求知者需要什么，陷入自嗨的窘境。
>
> `赛泰先生`力求将这两种矛盾平衡：一方面将丰富的实战经验、实战场景、技术理解、技术思考分享给大家；另一方面也将缜密的逻辑能力分享给大家。

### 资料获取方式

源代码资料有偿获取，感兴趣添加微信`dream4s`了解获取。

已是VIP会员，并且处于有效期的朋友享受折扣价。

### 源代码模块清单

| 模块名   | 实战内容   |
| ---- | ---- |
| 04001-springboot-mybatis-plus-intro |快速入门 |
| 04002-springboot-mybatis-plus-auto-fill-metainfo |字段属性全局自动填充 |
| 04003-springboot-mybatis-plus-enum |字段枚举 |
| 04004-springboot-mybatis-plus-typehandle |类型处理器 处理复杂类型 |
| 04005-springboot-mybatis-plus-custom-method |自定义Mapper方法 |
| 04006-springboot-mybatis-plus-tenant | 多租户|
| 04007-springboot-mybatis-plus-active-record |AR |
| 04008-springboot-mybatis-plus-id-generator |自定义ID生成器 |
| 04009-springboot-mybatis-plus-id-string | 自定义ID生成器（字符串）|
| 04010-springboot-mybatis-plus-resultmap |处理复杂返回值 |
| 04011-springboot-mybatis-plus-pagehelper |整合pagehelper分页 |
| 04012-springboot-mybatis-plus-pagination |分页 |
| 04013-springboot-mybatis-plus-logic-delete |逻辑删除 |
| 04014-springboot-mybatis-plus-optimistic-locker |乐观锁 |
| 04015-springboot-mybatis-plus-wrapper | 查询条件构造器|
| 04016-springboot-mybatis-plus-execution-protect |执行保护 |
| 04017-springboot-mybatis-plus-sequence |oracle等数据库保持顺序 |
| 04020-springboot-mybatis-plus-dao-cache |DAO层添加缓存 |
| 04021-springboot-mybatis-plus-dao-cache-lock |DAO通过加锁添加缓存 |
| 04022-springboot-mybatis-plus-business-report |复杂业务报表开发 |
| 04023-springboot-mybatis-plus-lambda-groupby |聚合函数lambda版实战 |
| 04024-springboot-mybatis-plus-lambda-count-sum-avg-min-max |多种类型聚合函数lambda版实战 |
| 04025-springboot-mybatis-plus-id-to-name |lambda版将ID字段替换成name字段 |
| 04026-springboot-mybatis-plus-pk-backfill | 主键回填|
| 04027-springboot-mybatis-plus-lambda-increase-decrease |lambda版自增-自减实现 |
| 04028-springboot-mybatis-plus-lambda-increase-decrease2 |lambda版自增-自减实现（自定义框架实现） |
| 04029-springboot-mybatis-plus-redundance-field | lambda版冗余字段实现|
| 04030-springboot-mybatis-plus-redundance-field2 | lambda版冗余字段实现（自定义框架实现）|

