## SpringBoot失败重试框架

`失败重试`常见于同`第三方中间件`交互的场景，比如向`Redis`中保存数据，向`MQ`中写入消息，调用`第三方接口`等。

由于`网络扰动`，`IO异常`是客观存在的，有时候为了提高业务的可靠性，必须考虑失败重试。

通过设置`有限次失败重试`，`尽可能`确保重要业务执行成功。

### 使用步骤

添加依赖
```xml
<dependency>
    <groupId>org.springframework.retry</groupId>
    <artifactId>spring-retry</artifactId>
</dependency>
```

在SpringBoot启动类上添加注解`@EnableRetry`，开启失败重试功能。

##### 1、简单使用

在业务方法上添加注解`@Retryable(value = Exception.class)`，具体指定哪个方法需要失败重试。

```java
@Retryable(value = Exception.class)
public String sms2() {
    logger.info("开始执行！");
    throw new RuntimeException("执行异常");
}
```
简单使用意味着已经入门了`失败重试`，那么还有几个核心的关切点：
- 最大重试的次数是多少？无限次失败重试既不现实也没有意义，默认值是多少？
- 失败重试的`步长`机制是怎样子的？是`固定步长`还是`累增步长`，默认配置是多少？
- 如果超过了最大重试次数仍然失败，该怎么处理，默认值是什么？

##### 2、高级使用
- 最大重试次数
  
最大重试次数通过参数`maxAttempts`指定，默认值为`3`，如果不显示指定，则使用默认值。
- 失败重试步长

有两种方式指定失败重试步长，可根据业务自行选择。
  
第一种方式是失败后，每隔固定时间触发下一次尝试；第二种方式是逐步递增间隔时间的方式触发下一次尝试。
