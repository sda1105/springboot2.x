### 基于MybatisPlus内置AES工具包实现全局配置文件密码加密

对于配置文件敏感信息加密，有助于系统安全的提升，特别是涉及到生产环境的敏感信息。

##### 1、引入依赖
```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.2</version>
</dependency>
```
如果您的项目默认采用MybatisPlus作为技术组件，那么此步骤可以忽略。

##### 2、加密目标字段

理论上是支持所有字段加密的，考虑到实际情况，我们仅对与密码相关的字段进行加密。

`spring.datasource.password`和`spring.redis.password`

使用如下方法对原密码明文加密
```java
public class DemoPassword {
    /**
     * 密钥随意设置，应妥善保存
     */
    private final static String secret = "d1104d7c3b616f0b";
    public static void main(String[] args) {
        String password = "123456";
        System.out.println(AES.encrypt(password, secret));
    }
}
```
需要注意的是，密钥随意设置、应妥善保存，生产环境项目部署时会再次用到。

#### 3、配置文件前后变化
明文密码
```yml
spring:
  datasource:
    password: 123456
```
密文密码
```yml
spring:
  datasource:
    password: mpw:J2frhfCBquXqKoraqzr1Hg==
```

#### 4、启动项目
在命令行中指定生成密码时的`密钥`，程序运行时自动解析。
```bash
java -jar demo.jar --mpw.key=d1104d7c3b616f0b --spring.profiles.active=prod
```

##### 5、小结

密码加密是以牺牲方便性置换安全的提升，特别是部分项目喜欢将生产环境配置到项目中，随着人员流动，项目的不稳定性因素增大，此时需要考虑密码加密。

如果生产环境配置文件仅有运维管理，那么使用明文未尝不可，毕竟加密解密也比较麻烦。