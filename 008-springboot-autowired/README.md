## 为什么高版本的SpringBoot不推荐使用`@Autowired`属性注入

当在使用进行`@Autowired`属性注入时，idea会贴心提示`Field injection is not recommended`，这究竟是怎么回事呢？

### Java对象的创建过程
下面是一个普通的Java类，省略构造器和set/get方法，下面来讨论Java对象创建过程，从而为本主题内容做铺垫。
```java
public class User {
    private Long userId;
    private String userName;
    private Integer age = 12;
}
```
代码`User user = new User(1,"admin",12);`究竟发生了什么事？

![img.png](img.png)

下面通过通俗的语言分解对象创建中的核心过程（忽略不重要的部分）

- 在`栈内存`空间分配名为`user`的内存
- 在`堆内存`空间以`User`类为模版分配内存空间
- 成员变量初始化`零值`
- 成员变量初始化
- `init`初始化

在执行完init方法之后，便可以通过`get方法`访问到具体的成员变量值。

### `@Autowired`执行时刻
`@Autowired`本质是通过反射类进行的属性注入的，因此执行时刻是对象创建完成之后。

---

### Spring推荐以`构造器`的方式初始化成员变量

```java
@Service
public class DeptService {
    
    private Environment environment;
    
    /**
     * 通过传统的方式给成员变量赋值
     */
    public DeptService(Environment environment) {
        this.environment = environment;
    }
}
```

#### 优点
- 项目启动时性能的提升
使用构造器替换以反射为基础的成员变量初始化，最直接的收益是项目启东市性能的提升
  
~~- 回避循环依赖~~
