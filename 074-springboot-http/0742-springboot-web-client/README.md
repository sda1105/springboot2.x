## WebClient非阻塞网络调用

一、基本介绍

1，什么是 WebClient

从 Spring 5 开始，Spring 中全面引入了 Reactive 响应式编程。而 WebClient 则是 Spring WebFlux 模块提供的一个非阻塞的基于响应式编程的进行 Http 请求的客户端工具。

由于 WebClient 的请求模式属于异步非阻塞，能够以少量固定的线程处理高并发的 HTTP 请求。因此，从 Spring 5 开始，HTTP 服务之间的通信我们就可以考虑使用 WebClient 来取代之前的 RestTemplate。

2，WebClient 的优势

（1）与 RestTemplate 相比，WebClient 有如下优势：

- 非阻塞，Reactive 的，并支持更高的并发性和更少的硬件资源。
- 提供利用 Java 8 lambdas 的函数 API。
- 支持同步和异步方案。
- 支持从服务器向上或向下流式传输。

（2）RestTemplate 不适合在非阻塞应用程序中使用，因此 Spring WebFlux 应用程序应始终使用 WebClient。在大多数高并发场景中，WebClient 也应该是 Spring MVC
中的首选，并且用于编写一系列远程，相互依赖的调用。

---

POST等其他的方法在与GET方法在使用如下方法的时候是一致的：

- block()阻塞获取响应结果的方法，subscribe()非阻塞异步结果订阅方法
- retrieve(）获取HTTP响应体，exchange()除了获取HTTP响应体，还可以获取HTTP 状态码、headers、cookies等HTTP报文信息。
- 使用Mono接收单个对象的响应结果，使用Flux接收集合类对象的响应结果。
- 占位符语法传参方式