## RabbitMQ 手动确认

默认情况下，RabbitMQ是自动确认，开启手动确认需要添加如下配置

```properties
# 默认是simple（可以不配置）
spring.rabbitmq.listener.type=simple
# 默认是自动确认
spring.rabbitmq.listener.simple.acknowledge-mode=manual
```

