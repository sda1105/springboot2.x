## RabbitMQ 批量发送消息

所谓批量， 就是spring 将多条message重新组成一条message, 发送到mq, 从mq接受到这条message后，在重新解析成多条message

### 批量发送消息

批量发送消息相较于单条发送消息的区别是`消息的合并与拆分`，从而减少确认的网络请求次数，提高吞吐量。

