## 循环依赖

### 2.6.0版本开始默认禁止了循环依赖
由于默认禁止了循环依赖，升级到SpringBoot 2.6.0版本时，如果项目代码中存在循环依赖，那么启动运行会直接报错。

```text
The dependencies of some of the beans in the application context form a cycle:
```

#### 1、解决方式一
```yml
spring:
  main:
    allow-circular-references: true
```
方式一是解决问题最快的方式，但不是做好的方式，原因是Spring认为循环依赖是一个糟糕的设计，在后续版本中会逐步抛弃。

#### 2、解决方式二


#### 3、解决方式三
