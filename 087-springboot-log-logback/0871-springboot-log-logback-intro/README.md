## logback日志

- SpringBoot默认使用logback日志

一般来说SpringBoot均是Web项目，因此会引入Web依赖，内部预装日志模块，不需要显示引入日志依赖。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<!--不需要显示引入 显示引入也没有影响 注意版本号一致-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-logging</artifactId>
</dependency>
```

- 默认不添加任何配置使用`控制台`输出日志

控制台打印日志适合在开发阶段使用，项目上生产时需要单独配置其它内容，包含但不亚于日志的存储、日志的分类、日志的轮转等等。