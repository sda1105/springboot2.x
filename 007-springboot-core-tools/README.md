## Spring常用工具类
Spring体系常用工具类在核心包里，使用SpringBoot会自动引入。

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-core</artifactId>
</dependency>
```
版本号需单独配置。

### StreamUtils

`StreamUtils`用于处理`InputStream`和`OutputStream`与流相关的内容。

![img.png](img.png)

