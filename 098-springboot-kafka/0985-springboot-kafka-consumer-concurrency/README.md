## Kafka并发消费

Kafka并发消费能够显著提高消费能力。

若想使用Kafka的并发消费能力，需要启用`topic`分区功能，否则默认一个分区仍然是串型消费。


### 自定义topic分区

```java
@Bean
public NewTopic newTopic() {
    // 新建分区数为2 副本数为1的topic
    return new NewTopic(DemoProducer.TOPIC, 2, (short) 1);
}
```

通过上述方式，便可以通过Java代码操作topic分区。

### 并发消费

```java
/**
 * 客户端使用两个线程并发
 */
@KafkaListener(topics = DemoProducer.TOPIC, groupId = "A", concurrency = "2")
public void onMessage(User user) {
    logger.info("[onMessage]A[线程编号:{} 消息内容:{}]", Thread.currentThread().getId(), user);
}
```

通过注解`KafkaListener`参数指定并发数，建议并发数与分区数量保持一致。

也可以通过全局配置文件统一配置
```yml
spring:
  kafka:
    listener:
      concurrency: 2
```